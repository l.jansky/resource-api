import api from '../api';

const mailService = {
	sendMail: () => Promise.resolve({ ok: 1 })
};

export default async (getStorage, getResource) => {
	return {
		appConfig: {
			auth: {}
		},
		services: {
			getStorage,
			getResource,
			mailService,
			getPublicRoleId: () => 6
		},
		api: api as any
	};
};
