import KoaRouter from 'koa-router';
import servicesApi from './services-api';

const router = new KoaRouter();

router.use('/services', servicesApi.routes());

export default router;
