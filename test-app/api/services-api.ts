import KoaRouter from 'koa-router';
import R from 'ramda';

const router = new KoaRouter();

router.get('/', async function(ctx) {
	ctx.body = R.keys(ctx.services);
});

export default router;
