# Common behavior for (almost) all endpoints

There are 3 types of endpoints now
* /api/resource/:resourceName - endpoints for administration - authentication is needed
* /api/public/:resourceName - public endpoints (only read only) - without authentication
* /api/custom/:name - there can be also custom non-resource endpoints

## GET method for reading data in format
* /api/resource/:resourceName - for collection of items
* /api/resource/:resourceName/:id - for single item by id

### For collections

Collections are returned as arrays of objects (items), request for collection can have following query parameters:

#### Format
/api/resource/:resourceName?format=id,name

It defines which attributes from item object will be returned (for every item in collection)
* Attributes are separated by ','
	* /api/custom/event?format=id,name
* It can also return deep parameters in related objects (as sub objects)
	* /api/custom/event?format=id,name,place.name
* If there can be more related objects, it will return them as array (every object in the array will be formatted)
	* /api/custom/event?format=id,name,tournament.name will return:
	```
	[{
		id: 1,
		name: 'event name',
		tournament: [{
			name: 'singles'
		}, {
			name: 'doubles'
		}]
	}]
	```
* If without format, it will return all allowed attributes of base object (without relations)
* (TBD) Can use '*' in format to return all allowed attributes of base object (without relations)
	* /api/custom/event?format=*,tournament.name - it will return base attributes of event + tournament array with name

##### Format functions
/api/resource/:resourceName?format=id,date(date_start)

* only functions for formatting dates are available now
	* date
	* dateTime
* it wraps format attribute (is first parameter of function, other parameters are for formating and depends on function)
* '*' can't be used in format function
* if function is used without name, it will use object format function as default
	* first parameter is name of field which should be formatted, other parameters are fields which should be returned by formatting (if none of them are supplied, whole entity without formatting is returned)
	* /api/custom/event?format=id,name,(place,id,name) is equivalent to /api/custom/event?format=id,name,place.id,place.name
* functions can be nested
	* /api/custom/event?format=id,name,(tournament,*,(tableType,*,(manufacturer,*))) is equivalent to:
	* /api/custom/event?format=id,name,tournament.*,tournament.tableType.*,tournament.tableType.manufacturer.*

#### Filters

* Filters are query parameters with prefix f.
	* For example: /api/custom/event?f.place_id=4 will return all events with place_id=4
	* if there is no compare function used, equal is used as default (==)
	* if there are more filters defined, only items which meets all of them are returned (logical AND)
	* compare function wraps value of filter in url, for example:
		* /api/custom/tournament?f.attendance=gt(10) will return tournaments with attendance greater then 10
		* List of compare functions:
			* gt(value)
			* lt(value)
			* gte(value)
			* lte(value)
			* like(partOfText)

#### Paginating

* query parameter limit - how many items should be returned
* query parameter offset - from which item to start

#### Ordering

* query parameter order with attributes separated by ','
	* attribute without prefix - asc
	* attribute with '-' prefix - desc
* example:
	* /api/resource/:resourceName?order=attr1,-attr2


### For single records
/api/resource/:resourceName/:id

* returned record is defined by id in url
* format can be specified in the same style as for collections