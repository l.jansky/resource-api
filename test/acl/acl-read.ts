import chai from 'chai';
import R from 'ramda';
const assert = chai.assert;

import { getAclQuery } from '../../src/acl';
import * as dbTestConnection from '../dbTestConnection';

describe('Acl read', () => {
	beforeEach(async function() {
		await dbTestConnection.start(this);

		this.userResource = this.getResource('user');

		const acl = [
			{
				title: 'Private',
				system: 1
			},
			{
				title: 'Public',
				system: 1,
				public_read: 1
			}
		] as any;

		this.user1 = await this.userResource.insertOne({ acl });

		const customAcl = {
			title: 'Custom',
			system: 0,
			member_read: 1,
			aclUser: [
				{
					user_id: this.user1.id
				}
			]
		};

		this.user2 = await this.userResource.insertOne({ acl });
		this.user3 = await this.userResource.insertOne({
			acl: R.append(customAcl, acl)
		});

		const testResource = this.getResource('testEntity');

		this.user1privateEntity = await testResource.insertOne({
			test_number: 1,
			acl_id: this.user1.acl[0].id
		});
		this.user1publicEntity = await testResource.insertOne({
			test_number: 2,
			acl_id: this.user1.acl[1].id
		});

		this.user2privateEntity = await testResource.insertOne({
			test_number: 3,
			acl_id: this.user2.acl[0].id
		});
		this.user2publicEntity = await testResource.insertOne({
			test_number: 4,
			acl_id: this.user2.acl[1].id
		});
		this.user3memberEntity = await testResource.insertOne({
			test_number: 5,
			acl_id: this.user3.acl[2].id
		});
	});

	afterEach(async function() {
		await dbTestConnection.stop(this);
	});

	it('should find entities accessible by user1', async function() {
		const { storageConfig } = this.getStorage('testEntity');
		const aclQuery = getAclQuery(storageConfig, this.user1.id);
		const testResource = this.getResource('testEntity', aclQuery);
		const data = await testResource.findAll();

		assert.sameDeepMembers(data, [
			this.user1privateEntity,
			this.user1publicEntity,
			this.user2publicEntity,
			this.user3memberEntity
		]);
	});

	it('should find entities accessible by user2', async function() {
		const { storageConfig } = this.getStorage('testEntity');
		const aclQuery = getAclQuery(storageConfig, this.user2.id);
		const testResource = this.getResource('testEntity', aclQuery);
		const data = await testResource.findAll();

		assert.sameDeepMembers(data, [
			this.user1publicEntity,
			this.user2privateEntity,
			this.user2publicEntity
		]);
	});

	it('should find entities accessible by user3', async function() {
		const { storageConfig } = this.getStorage('testEntity');
		const aclQuery = getAclQuery(storageConfig, this.user3.id);
		const testResource = this.getResource('testEntity', aclQuery);
		const data = await testResource.findAll();

		assert.sameDeepMembers(data, [
			this.user1publicEntity,
			this.user2publicEntity,
			this.user3memberEntity
		]);
	});
});
