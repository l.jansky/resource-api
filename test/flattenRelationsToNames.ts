import { expect } from 'chai';
import flattenRelationsToNames from '../src/flattenRelationsToNames';
import { Relation } from '../src/types';

describe('FlattenRelationsToNames', () => {
	it('should return relation names from nested relations', () => {
		const relations: Relation[] = [
			{
				name: 'Relation1'
			},
			{
				name: 'Relation2',
				relations: [
					{
						name: 'NestedRelation1'
					},
					{
						name: 'NestedRelation2',
						relations: [
							{
								name: 'NestedRelation3'
							}
						]
					}
				]
			},
			{
				name: 'Relation3',
				relations: [
					{
						name: 'Relation2',
						relations: [
							{
								name: 'NestedRelation4'
							},
							{
								name: 'NestedRelation1'
							}
						]
					}
				]
			}
		];

		const expected = [
			'Relation1',
			'Relation2',
			'Relation3',
			'NestedRelation1',
			'NestedRelation2',
			'NestedRelation3',
			'NestedRelation4'
		];
		const relationNames = flattenRelationsToNames(relations);
		expect(Array.from(relationNames)).to.have.members(expected);
	});
});
