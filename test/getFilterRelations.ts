import chai from 'chai';
import getFilterRelations from '../src/getFilterRelations';

const assert = chai.assert;

describe('getFilterRelations', () => {
	it('should get empty relations from empty filter', () => {
		const filter = [];
		const relations = getFilterRelations(filter);
		const expected = [];
		assert.deepEqual(relations, expected);
	});

	it('should get empty relations from filter which has no deep fields', () => {
		const filter = [
			{
				operator: 'gt',
				field: 'test_number',
				value: 1
			},
			{
				operator: 'lte',
				field: 'test_number',
				value: 3
			}
		];

		const relations = getFilterRelations(filter);
		const expected = [];
		assert.deepEqual(relations, expected);
	});

	it('should get relations from filter with dotted deep fields', () => {
		const filter = [
			{
				operator: 'gt',
				field: 'deep1.test_number1',
				value: 1
			},
			{
				operator: 'lte',
				field: 'deep1.test_number2',
				value: 3
			},
			{
				operator: 'lte',
				field: 'deep2.test_number3',
				value: 3
			}
		];

		const relations = getFilterRelations(filter);

		const expected = [
			{
				name: 'deep1',
				relations: []
			},
			{
				name: 'deep2',
				relations: []
			}
		];

		assert.deepEqual(relations, expected);
	});

	it('should get relations from filter with deep operators', () => {
		const filter = [
			{
				operator: 'has',
				field: 'deep',
				value: [
					{
						operator: 'eq',
						field: 'test',
						value: 1
					}
				]
			},
			{
				operator: 'and',
				value: [
					{
						operator: 'eq',
						field: 'test',
						value: 2
					}
				]
			}
		];

		const relations = getFilterRelations(filter);

		const expected = [
			{
				name: 'deep',
				relations: []
			}
		];

		assert.deepEqual(relations, expected);
	});

	it('should get relations from filter with nested deep operators', () => {
		const filter = [
			{
				operator: 'has',
				field: 'deep',
				value: [
					{
						operator: 'eq',
						field: 'deeper1.test',
						value: 2
					},
					{
						operator: 'has',
						field: 'deeper2',
						value: [
							{
								operator: 'eq',
								field: 'test',
								value: 2
							}
						]
					}
				]
			}
		];

		const relations = getFilterRelations(filter);

		const expected = [
			{
				name: 'deep',
				relations: [
					{
						name: 'deeper1',
						relations: []
					},
					{
						name: 'deeper2',
						relations: []
					}
				]
			}
		];

		assert.deepEqual(relations, expected);
	});

	it('should get merged deep relations', () => {
		const filter = [
			{
				operator: 'eq',
				field: 'deep.test',
				value: 1
			},
			{
				operator: 'has',
				field: 'deep',
				value: [
					{
						operator: 'eq',
						field: 'deeper.test',
						value: 2
					}
				]
			}
		];

		const relations = getFilterRelations(filter);

		const expected = [
			{
				name: 'deep',
				relations: [
					{
						name: 'deeper',
						relations: []
					}
				]
			}
		];

		assert.deepEqual(relations, expected);
	});
});
