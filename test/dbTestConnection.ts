import path from 'path';
import {
	getStorageFactory,
	loadStorageConfigs,
	loadXmlConfigsFromPaths,
	mockAdapter
} from '@prejt/db-storage';
import sqlAdapter from '@prejt/db-storage-sql';
import dbTest from '@prejt/db-test';
import { getCoreModelsPath, getResourceFactory } from '../src';

const dbAdapter = process.env.DB_ADAPTER || 'mock';

const connectionInfo = {
	host: 'localhost',
	user: 'root',
	password: 'password',
	database: 'db',
	debug: false
};

export const init = async ctx => {
	if (dbAdapter === 'sql') {
		ctx.timeout(20000);
		await dbTest.init(
			path.join(__dirname, '..', 'test-db'),
			connectionInfo,
			false
		);
	}
};

export const start = async ctx => {
	const paths = [getCoreModelsPath(), `${__dirname}/../test-app/models`];

	const fixturePaths = [`${__dirname}/../test-app/fixtures`];

	const storageConfigs = await loadStorageConfigs(paths);
	const fixtures = await loadXmlConfigsFromPaths(fixturePaths);

	switch (dbAdapter) {
		case 'sql':
			ctx.timeout(20000);
			ctx.container = await dbTest.start();
			ctx.connection = sqlAdapter.getDbConnection(connectionInfo);

			ctx.getStorage = await getStorageFactory(
				storageConfigs,
				fixtures,
				sqlAdapter.createAdapter(ctx.connection)
			);
			break;
		default:
			ctx.getStorage = await getStorageFactory(
				storageConfigs,
				fixtures,
				mockAdapter.createAdapter({})
			);
			break;
	}

	ctx.getResource = getResourceFactory(ctx.getStorage);
};

export const stop = async ctx => {
	if (dbAdapter === 'sql') {
		await dbTest.remove(ctx.container);
	}
};
