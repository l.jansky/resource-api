import chai from 'chai';
import {
	generatePassword,
	hashPassword,
	verifyPassword,
	setPasswordForUser,
	getUserVerifiedByPassword,
	getUserByEmail,
	getUserByFbId,
	getOrCreateFbUser
} from '../src/auth';
import * as dbTestConnection from './dbTestConnection';
import { FbUser } from '../src';

const expect = chai.expect;

describe('Auth service', () => {
	const correctPassword = 'testPassword';
	const incorrectPassword = `${correctPassword}incorrect`;
	const email = 'test@test.cz';

	beforeEach(async function() {
		await dbTestConnection.start(this);
		const userResource = this.getResource('user');
		const user = await userResource.insertOne({
			email
		});
		await setPasswordForUser<
			any
		>(this.getResource, user.id, true, correctPassword);
	});

	afterEach(async () => {
		await dbTestConnection.stop(this);
	});

	it('should generate password', () => {
		const generatedPassword = generatePassword();
		expect(generatedPassword).to.be.a('string');
	});

	it('should hash password and verify', () => {
		const passwordInfo = hashPassword(correctPassword);
		expect(passwordInfo).to.have.all.keys([
			'password_hash',
			'salt',
			'iterations'
		]);

		expect(verifyPassword(passwordInfo, incorrectPassword)).to.equal(false);
		expect(verifyPassword(passwordInfo, correctPassword)).to.equal(true);
	});

	describe('Password for user', async () => {
		it('should get user by email', async function() {
			const user = await getUserByEmail(this.getResource, email);
			expect(user)
				.to.have.property('email')
				.to.equal(email);
		});

		it('should get verified user by correct password', async function() {
			const verifiedUser = await getUserVerifiedByPassword(this.getResource)(
				email
			)(correctPassword);
			expect(verifiedUser)
				.to.have.property('email')
				.to.equal(email);
		});

		it('should not get user when verified with incorrect password', async function() {
			const notVerifiedUser = await getUserVerifiedByPassword(this.getResource)(
				email
			)(incorrectPassword);
			expect(notVerifiedUser).to.equal(null);
		});
	});

	describe('Facebook login', () => {
		it('should not find user if not in loginFacebook table', async function() {
			const user = await getUserByFbId(this.getResource, 'does-not-exist');
			expect(user).to.equal(null);
		});

		it('should register user with fbId and get him', async function() {
			const fbProfile: FbUser = {
				id: 'fbId',
				email: 'fb@example.com',
				first_name: '',
				last_name: ''
			};
			const newUser = await getOrCreateFbUser(this.getResource, fbProfile);
			const user = await getUserByFbId(this.getResource, 'fbId');
			expect(user.id).to.equal(newUser.id);

			await getOrCreateFbUser(this.getResource, {
				...fbProfile,
				email,
				id: 'newFbId'
			});
			const existingUser = await getUserByFbId(this.getResource, 'newFbId');
			expect(existingUser.email).to.equal(email);
		});
	});
});
