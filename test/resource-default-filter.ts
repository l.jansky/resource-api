import chai from 'chai';
import * as dbTestConnection from './dbTestConnection';

const assert = chai.assert;

describe('Resource default filter', () => {
	beforeEach(async function() {
		await dbTestConnection.start(this);

		this.testResource = this.getResource('testEntity', [
			{
				operator: 'gt',
				field: 'test_number',
				value: 1
			}
		]);

		const entitiesToInsert = [
			{
				test_number: 1
			},
			{
				test_number: 2
			},
			{
				test_number: 3
			}
		];

		this.insertedEntities = [];
		for (const entity of entitiesToInsert) {
			const ent = await this.testResource.insertOne(entity);
			this.insertedEntities.push(ent);
		}
	});

	afterEach(async function() {
		await dbTestConnection.stop(this);
	});

	it('should find entities filtered by default filter', async function() {
		const data = await this.testResource.findAll();
		assert.deepEqual(data, [
			this.insertedEntities[1],
			this.insertedEntities[2]
		]);
	});

	it('should find entities filtered by default filter and standard filter', async function() {
		const filter = [
			{
				operator: 'lt',
				field: 'test_number',
				value: 3
			}
		];
		const data = await this.testResource.findAll({ filter });
		assert.deepEqual(data, [this.insertedEntities[1]]);
	});
});
