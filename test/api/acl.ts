import getApiTestUtil from '../../src/test-utils/apiTestUtil';
import { appFactory } from '../../src';
import getTestConfig from '../../test-app/config/test.config';
import * as dbTestConnection from '../dbTestConnection';

describe('ACL custom api', () => {
	beforeEach(async function() {
		await dbTestConnection.start(this);
		const testConfig = await getTestConfig(this.getStorage, this.getResource);

		const app = await appFactory(testConfig);
		this.api = getApiTestUtil(app, testConfig);

		this.user1 = await this.api.login([1]);
		this.user2 = await this.api.login([1]);
		this.user3 = await this.api.login([1]);

		// members of aclForEditing should be able to edit editedAcl

		this.aclForEditing = await this.api.post(
			'/api/acl',
			200,
			this.user1.token,
			{
				title: 'test1',
				read: 'member',
				write: 'member',
				members: [this.user2.id]
			}
		);

		this.editedAcl = await this.api.post('/api/acl', 200, this.user1.token, {
			title: 'test2',
			read: 'owner',
			write: 'owner',
			acl_id: this.aclForEditing.id
		});
	});

	afterEach(async function() {
		await dbTestConnection.stop(this);
		this.api.stop();
	});

	it('Should be possible to update own ACL', async function() {
		await this.api.put(`/api/acl/${this.editedAcl.id}`, 200, this.user1.token, {
			title: 'test3'
		});

		await this.api.put(
			`/api/acl/${this.aclForEditing.id}`,
			200,
			this.user1.token,
			{
				title: 'test4'
			}
		);
	});

	it("Should not be possible to update other's user ACL", async function() {
		await this.api.put(
			`/api/acl/${this.aclForEditing.id}`,
			403,
			this.user2.token,
			{
				title: 'test2'
			}
		);
	});

	it.skip('Should be possible to update members of ACL which has allowed updates by members of another ACL', async function() {
		await this.api.put(`/api/acl/${this.editedAcl.id}`, 403, this.user2.token, {
			title: 'test3'
		});

		await this.api.put(`/api/acl/${this.editedAcl.id}`, 200, this.user2.token, {
			members: [this.user3.id]
		});

		// const permissions = await this.api.get('/api/acl', 200, this.user1);
		// console.log(permissions);
	});
});
