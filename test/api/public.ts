import { expect } from 'chai';
import { omit } from 'ramda';
import { appFactory } from '../../src';
import getApiTestUtil, {
	ApiTestUtil,
	TestLoggedUser
} from '../../src/test-utils/apiTestUtil';
import getTestConfig from '../../test-app/config/test.config';
import * as dbTestConnection from '../dbTestConnection';

describe('Public API', () => {
	let api: ApiTestUtil;
	let user: TestLoggedUser;
	let privateEntity;
	let publicEntity;

	beforeEach(async function() {
		await dbTestConnection.start(this);
		const testConfig = await getTestConfig(this.getStorage, this.getResource);

		const app = await appFactory(testConfig);
		api = getApiTestUtil(app, testConfig);
		user = await api.login([1]);

		privateEntity = await api.post(
			'/api/resource/testEntity',
			200,
			user.token,
			{
				title: 'test1',
				testEntityRelated: [{}, {}],
				acl_id: user.privateAcl.id
			}
		);

		publicEntity = await api.post('/api/resource/testEntity', 200, user.token, {
			title: 'test1',
			testEntityRelated: [
				{
					title: 'related1',
					testEntityRelated2: [{}, {}]
				},
				{},
				{}
			],
			acl_id: user.publicAcl.id
		});
	});

	afterEach(async function() {
		await dbTestConnection.stop(this);
		api.stop();
	});

	it('should not be possible to get private entity from allowed resource', async () => {
		await api.get(`/api/public/testEntity/${privateEntity.id}`, 404);
	});

	it('should be possible to get public entity from allowed resource', async () => {
		await api.get(`/api/public/testEntity/${publicEntity.id}`, 200);
	});

	it('should not be possible to get public entity from resource which is not allowed', async () => {
		await api.get(
			`/api/public/testEntityRelated2/${publicEntity.testEntityRelated[0].testEntityRelated2[0].id}`,
			403
		);
	});

	it('should not be possible to get entity which is not allowed by format', async () => {
		await api.get(`/api/public/testEntity/${publicEntity.id}`, 403, undefined, {
			format: 'id,(testEntity2),(testEntityRelated,id,(testEntityRelated2))'
		});
	});

	it('should be possible to list only public entities from allowed resource', async () => {
		const result = await api.get(`/api/public/testEntity`, 200);
		expect(result).to.deep.equal([omit(['testEntityRelated'], publicEntity)]);
	});

	it('should not be possible to list entities from not allowed resource', async () => {
		await api.get(`/api/public/testEntityRelated2`, 403);
	});

	it('should not be possible to list entities with subEntities from not allowed resources', async () => {
		await api.get(`/api/public/testEntity`, 403, undefined, {
			format: 'id,(testEntityRelated,(testEntityRelated2))'
		});
	});

	it('should be possible to get sub-entity of public entity', async () => {
		await api.get(
			`/api/public/testEntityRelated/${publicEntity.testEntityRelated[0].id}`,
			200
		);
	});

	it('should not be possible to get sub-entity of private entity', async () => {
		await api.get(
			`/api/public/testEntityRelated/${privateEntity.testEntityRelated[0].id}`,
			404
		);
	});
});
