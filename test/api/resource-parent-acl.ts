import { omit } from 'ramda';
import {
	ApiContext,
	UserContext,
	EntityContext,
	testEntityPermissionsWithApiContext,
	Expectations,
	TestConfig
} from '../../src/test-utils/test-entity-permissions';
import { appFactory } from '../../src';
import getApiTestUtil from '../../src/test-utils/apiTestUtil';
import getTestConfig from '../../test-app/config/test.config';
import * as dbTestConnection from '../dbTestConnection';

describe('Resource parent acl', () => {
	let privateEntity;
	let publicEntity;

	const apiContext: ApiContext = {};
	const ownerContext: UserContext = {};
	const memberContext: UserContext = {};
	const nonMemberContext: UserContext = {};

	const privateRelatedEntityContext: EntityContext = {
		resource: 'testEntityRelated'
	};

	const memberRelatedEntityContext: EntityContext = {
		resource: 'testEntityRelated'
	};

	const publicRelatedEntityContext: EntityContext = {
		resource: 'testEntityRelated'
	};

	const privateDeepRelatedEntityContext: EntityContext = {
		resource: 'testEntityRelated2'
	};

	const memberDeepRelatedEntityContext: EntityContext = {
		resource: 'testEntityRelated2'
	};

	const publicDeepRelatedEntityContext: EntityContext = {
		resource: 'testEntityRelated2'
	};

	const testEntityPermissions = testEntityPermissionsWithApiContext(apiContext);

	beforeEach(async function() {
		await dbTestConnection.start(this);
		const testConfig = await getTestConfig(this.getStorage, this.getResource);

		const app = await appFactory(testConfig);
		apiContext.api = getApiTestUtil(app, testConfig);

		ownerContext.user = await apiContext.api.login([1]);
		memberContext.user = await apiContext.api.login([1]);
		nonMemberContext.user = await apiContext.api.login([1]);

		privateEntity = await apiContext.api.post(
			'/api/resource/testEntity',
			200,
			ownerContext.user.token,
			{
				title: 'private',
				testEntityRelated: [
					{
						title: 'relatedPrivate',
						testEntityRelated2: [
							{
								title: 'deepRelatedPrivate'
							}
						]
					}
				],
				acl_id: ownerContext.user.privateAcl.id
			}
		);

		privateRelatedEntityContext.entity = omit(
			['testEntityRelated2'],
			privateEntity.testEntityRelated[0]
		);
		privateDeepRelatedEntityContext.entity =
			privateEntity.testEntityRelated[0].testEntityRelated2[0];

		publicEntity = await apiContext.api.post(
			'/api/resource/testEntity',
			200,
			ownerContext.user.token,
			{
				title: 'public',
				testEntityRelated: [
					{
						title: 'relatedPublic',
						testEntityRelated2: [
							{
								title: 'deepRelatedPublic'
							}
						]
					}
				],
				acl_id: ownerContext.user.publicAcl.id
			}
		);

		publicRelatedEntityContext.entity = omit(
			['testEntityRelated2'],
			publicEntity.testEntityRelated[0]
		);
		publicDeepRelatedEntityContext.entity =
			publicEntity.testEntityRelated[0].testEntityRelated2[0];
	});

	afterEach(async function() {
		await dbTestConnection.stop(this);
		apiContext.api.stop();
	});

	const writeExpectations: Expectations = {
		read: true,
		list: true,
		listWritable: true,
		update: true,
		delete: true
	};

	const readExpectations: Expectations = {
		read: true,
		list: true,
		listWritable: false,
		update: false,
		delete: false
	};

	const hiddenExpectations: Expectations = {
		read: false,
		list: false,
		listWritable: false,
		update: false,
		delete: false
	};

	const testConfigs: TestConfig[] = [
		{
			label: 'public parent',
			entityContext: publicRelatedEntityContext,
			ownerExpectations: writeExpectations,
			nonMemberExpectations: readExpectations
		},
		{
			label: 'private parent',
			entityContext: privateRelatedEntityContext,
			ownerExpectations: writeExpectations,
			nonMemberExpectations: hiddenExpectations
		},
		{
			label: 'public parent (deep)',
			entityContext: publicDeepRelatedEntityContext,
			ownerExpectations: writeExpectations,
			nonMemberExpectations: readExpectations
		},
		{
			label: 'private parent (deep)',
			entityContext: privateDeepRelatedEntityContext,
			ownerExpectations: writeExpectations,
			nonMemberExpectations: hiddenExpectations
		}
	];

	testConfigs.forEach(
		({ label, entityContext, ownerExpectations, nonMemberExpectations }) => {
			describe(`Test permissions for entity with ${label}`, () => {
				describe('as owner', () => {
					testEntityPermissions(ownerContext, entityContext, ownerExpectations);
				});

				describe('as non-member', () => {
					testEntityPermissions(
						nonMemberContext,
						entityContext,
						nonMemberExpectations
					);
				});
			});
		}
	);
});
