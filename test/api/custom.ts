import chai from 'chai';
import { appFactory } from '../../src';
import getApiTestUtil from '../../src/test-utils/apiTestUtil';
import getTestConfig from '../../test-app/config/test.config';
import * as dbTestConnection from '../dbTestConnection';

const assert = chai.assert;

describe('Custom api routes', () => {
	beforeEach(async function() {
		await dbTestConnection.start(this);
		const testConfig = await getTestConfig(this.getStorage, this.getResource);
		const app = await appFactory(testConfig);
		this.api = getApiTestUtil(app);
	});

	afterEach(async function() {
		await dbTestConnection.stop(this);
		this.api.stop();
	});

	it('should have services available', async function() {
		const services = await this.api.get('/api/custom/services', 200);
		assert.include(services, 'getResource');
	});
});
