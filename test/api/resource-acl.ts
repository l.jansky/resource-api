import { appFactory } from '../../src';
import getApiTestUtil from '../../src/test-utils/apiTestUtil';
import getTestConfig from '../../test-app/config/test.config';
import * as dbTestConnection from '../dbTestConnection';

describe('Resource api ACL', () => {
	beforeEach(async function() {
		await dbTestConnection.start(this);
		const testConfig = await getTestConfig(this.getStorage, this.getResource);

		const app = await appFactory(testConfig);
		this.api = getApiTestUtil(app, testConfig);

		this.user1 = await this.api.login([1]);
		this.user2 = await this.api.login([2]);
		this.user3 = await this.api.login([1, 2]);

		this.inserted = await this.api.post(
			'/api/resource/testEntity',
			200,
			this.user1.token,
			{
				title: 'test1',
				testEntityRelated: [{}, {}, {}]
			}
		);
	});

	afterEach(async function() {
		await dbTestConnection.stop(this);
		this.api.stop();
	});

	it('Should read only resource which is in users role permissions', async function() {
		await this.api.get('/api/resource/testEntity2', 200, this.user1.token);
		await this.api.get('/api/resource/testEntity2', 403, this.user2.token);
		await this.api.get('/api/resource/testEntity2', 200, this.user3.token);
	});

	it('Should be possible to create entity only with user role permissions', async function() {
		await this.api.post('/api/resource/testEntity2', 200, this.user1.token, {});
		await this.api.get('/api/resource/testEntity2', 403, this.user2.token, {});
		await this.api.get('/api/resource/testEntity2', 200, this.user3.token, {});
	});

	it('Should not be possible to create acl by resource api', async function() {
		await this.api.post('/api/resource/acl', 403, this.user1.token, {
			title: 'ggg',
			member_read: 1
		});
	});

	it('Should not be possible to update acl by resource api', async function() {
		await this.api.put(
			`/api/resource/acl/${this.user1.publicAcl.id}`,
			403,
			this.user1.token,
			{ title: 'ggg1' }
		);
		await this.api.post('/api/resource/aclUser', 403, this.user1.token, {
			acl_id: this.user1.publicAcl.id,
			user_id: this.user1.id
		});
	});

	it.skip('Should get users permissions', async function() {
		try {
			const user = this.fixtures.get('user', 0);
			await this.api.get('/api/acl', 200, user);
			// console.log(permissions);
		} catch (err) {
			console.log(err);
		}
	});

	it("Should not be possible to create entity with other's user acl", async function() {
		await this.api.post('/api/resource/testEntity', 403, this.user1.token, {
			title: 'ggg',
			acl_id: this.user2.publicAcl.id
		});
	});

	it('Should be possible to create entity with own public acl', async function() {
		const inserted = await this.api.post(
			'/api/resource/testEntity',
			200,
			this.user1.token,
			{
				title: 'ggg',
				acl_id: this.user1.publicAcl.id
			}
		);

		await this.api.get(
			`/api/resource/testEntity/${inserted.id}`,
			200,
			this.user2.token
		);
	});

	it('Should not be possible to insert new acl while creating entity', async function() {
		await this.api.post('/api/resource/testEntity', 403, this.user1.token, {
			title: 'ggg',
			acl: {
				title: 'aclTest'
			}
		});
	});

	it.skip('Should not be possible to create user inserted to aclUser resource directly', async function() {
		await this.api.post('/api/resource/user', 403, this.user1, {
			first_name: 'test',
			last_name: 'test',
			email: 'ggg',
			aclUser: {
				acl_id: this.loggedUser1.acl_id
			}
		});
	});

	it('Should be possible to change acl on own entity', async function() {
		await this.api.get(
			`/api/resource/testEntity/${this.inserted.id}`,
			404,
			this.user2.token
		);
		await this.api.put(
			`/api/resource/testEntity/${this.inserted.id}`,
			200,
			this.user1.token,
			{
				acl_id: this.user1.publicAcl.id
			}
		);
		await this.api.get(
			`/api/resource/testEntity/${this.inserted.id}`,
			200,
			this.user2.token
		);
	});

	it("Should not be possible to update entity to have another's user acl", async function() {
		await this.api.put(
			`/api/resource/testEntity/${this.inserted.id}`,
			403,
			this.user1.token,
			{
				acl_id: this.user2.publicAcl.id
			}
		);
	});

	it("should be possible to create and update entity with another's user acl where the current user is member and members can write", async function() {
		const aclWithMember = await this.api.post(
			'/api/acl',
			200,
			this.user1.token,
			{
				title: 'test1',
				read: 'member',
				write: 'member',
				members: [this.user3.id]
			}
		);

		const inserted = await this.api.post(
			'/api/resource/testEntity',
			200,
			this.user3.token,
			{
				title: 'ggg',
				acl_id: aclWithMember.id
			}
		);
	});

	it("should not be possible to create entity with another's user acl where the current user is member and members can not write", async function() {
		const aclWithMember = await this.api.post(
			'/api/acl',
			200,
			this.user1.token,
			{
				title: 'test1',
				read: 'member',
				write: 'owner',
				members: [this.user3.id]
			}
		);

		await this.api.post('/api/resource/testEntity', 403, this.user3.token, {
			title: 'ggg',
			acl_id: aclWithMember.id
		});
	});

	it('should be possible to update acl of own entity to acl where current user is member', async function() {
		const aclWithMember = await this.api.post(
			'/api/acl',
			200,
			this.user3.token,
			{
				title: 'test1',
				read: 'member',
				write: 'member',
				members: [this.user1.id]
			}
		);

		await this.api.put(
			`/api/resource/testEntity/${this.inserted.id}`,
			200,
			this.user1.token,
			{
				acl_id: aclWithMember.id
			}
		);
	});
});
