import sinon from 'sinon';
import * as auth from '../../src/auth';
import { appFactory } from '../../src';
import getApiTestUtil from '../../src/test-utils/apiTestUtil';
import getTestConfig from '../../test-app/config/test.config';
import * as dbTestConnection from '../dbTestConnection';

describe('Auth api routes', () => {
	const email = 'test@test.cz';
	const password = auth.generatePassword();

	beforeEach(async function() {
		await dbTestConnection.start(this);
		const testConfig = await getTestConfig(this.getStorage, this.getResource);
		const app = await appFactory(testConfig);
		this.api = getApiTestUtil(app);

		const userResource = testConfig.services.getResource('user');
		this.user = await userResource.insertOne({
			email,
			userRole: [{ role_id: 1 }]
		});

		await auth.setPasswordForUser<any>(
			this.getResource,
			this.user.id,
			false,
			password
		);
	});

	afterEach(async function() {
		await dbTestConnection.stop(this);
		this.api.stop();
	});

	it('should be forbidden to access protected api without access token', async function() {
		await this.api.get('/api/resource/test', 401);
	});

	it('should not be possible to get access token with wrong password', async function() {
		await this.api.post('/api/auth/login', 401, null, {
			email,
			password: `${password}wrong`
		});
	});

	it('should not be possible to get access token with wrong email', async function() {
		await this.api.post('/api/auth/login', 401, null, {
			email: `wrong${email}`,
			password
		});
	});

	it('should not be possible to get access token without credentials', async function() {
		await this.api.post('/api/auth/login', 401);
	});

	it('should not be possible to change password without access token', async function() {
		await this.api.put('/api/auth/password', 401, null, {});
	});

	describe('User is logged in with generated password', () => {
		let token;
		const newPassword = 'new_password';
		beforeEach(async function() {
			const userInfo = await this.api.post('/api/auth/login', 200, null, {
				email,
				password
			});
			token = userInfo.token;
		});

		it.skip('should be forbidden to access protected api without reseting password', async function() {
			await this.api.get('/api/resource/testEntity', 401, token);
		});

		it('should not be possible to change password without old one', async function() {
			await this.api.put('/api/auth/password', 400, token, {
				newPassword,
				newPasswordRepeat: newPassword
			});
		});

		it('should not be possible to change password without new one', async function() {
			await this.api.put('/api/auth/password', 400, token, {
				oldPassword: password
			});
		});

		it('should not be possible to change password with different newPassword and newPasswordRepeat', async function() {
			await this.api.put('/api/auth/password', 400, token, {
				oldPassword: password,
				newPassword,
				newPasswordRepeat: `${newPassword}diff`
			});
		});

		it('should not be possible to change password with incorrect oldPassword', async function() {
			await this.api.put('/api/auth/password', 400, token, {
				oldPassword: `${password}incorrect`,
				newPassword,
				newPasswordRepeat: newPassword
			});
		});

		describe('User has changed password', () => {
			beforeEach(async function() {
				await this.api.put('/api/auth/password', 200, token, {
					oldPassword: password,
					newPassword,
					newPasswordRepeat: newPassword
				});
			});

			it('should not be possible to login with old password', async function() {
				await this.api.post('/api/auth/login', 401, null, { email, password });
			});

			it('should be possible to access protected api with changed password', async function() {
				await this.api.get('/api/resource/testEntity', 200, token);
			});

			it('should be possible to login with new password', async function() {
				await this.api.post('/api/auth/login', 200, null, {
					email,
					password: newPassword
				});
			});
		});
	});

	it('should not generate password for non existing user', async function() {
		await this.api.post('/api/auth/password', 400, null, {
			email: 'non-existing@email.cz'
		});
	});

	it('should not generate password without email', async function() {
		await this.api.post('/api/auth/password', 400, null, {});
	});

	it('should generate new password for new user and email it', async function() {
		const spy = sinon.spy(auth, 'generatePassword');
		await this.api.post('/api/auth/password', 200, null, { email });
		const resetPassword = spy.returnValues[0];
		await this.api.post('/api/auth/login', 200, null, {
			email,
			password: resetPassword
		});
		(auth.generatePassword as any).restore();
	});
});
