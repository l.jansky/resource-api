import chai from 'chai';
import R from 'ramda';
import { appFactory } from '../../src';
import getApiTestUtil from '../../src/test-utils/apiTestUtil';
import getTestConfig from '../../test-app/config/test.config';
import * as dbTestConnection from '../dbTestConnection';

const assert = chai.assert;

describe('Resource api base', () => {
	beforeEach(async function() {
		await dbTestConnection.start(this);
		const testConfig = await getTestConfig(this.getStorage, this.getResource);

		const app = await appFactory(testConfig);
		this.api = getApiTestUtil(app, testConfig);

		this.user = await this.api.login([1]);

		this.inserted = await this.api.post(
			'/api/resource/testEntity',
			200,
			this.user.token,
			{
				title: 'test1',
				testEntityRelated: [{}, {}, {}]
			}
		);
	});

	afterEach(async function() {
		await dbTestConnection.stop(this);
		this.api.stop();
	});

	it('should not be possible to use non existing service', async function() {
		await this.api.get('/api/resource/non-existing', 404, this.user.token);
	});

	it('should get all data from existing resource', async function() {
		const data = await this.api.get(
			'/api/resource/testEntityRelated',
			200,
			this.user.token
		);
		assert.deepEqual(data, this.inserted.testEntityRelated);
	});

	it('should get data by id from existing resource', async function() {
		const data = await this.api.get(
			`/api/resource/testEntity/${this.inserted.id}`,
			200,
			this.user.token
		);
		assert.deepEqual(data, R.omit(['testEntityRelated'], this.inserted));
	});

	it('should get formatted data by id', async function() {
		const data = await this.api.get(
			`/api/resource/testEntity/${this.inserted.id}`,
			200,
			this.user.token,
			{ format: 'id,testEntityRelated.id' }
		);
		const expected = {
			id: this.inserted.id,
			testEntityRelated: R.map(R.pick(['id']), this.inserted.testEntityRelated)
		};
		assert.deepEqual(data, expected);
	});

	it('should get all formatted data', async function() {
		const data = await this.api.get(
			'/api/resource/testEntityRelated',
			200,
			this.user.token,
			{ format: 'id,testEntity.title' }
		);
		const expected = R.map(
			item => ({
				id: item.id,
				testEntity: { title: this.inserted.title }
			}),
			this.inserted.testEntityRelated
		);

		assert.deepEqual(data, expected);
	});

	it('should get filtered data using f. filters', async function() {
		const inserted2 = await this.api.post(
			'/api/resource/testEntity',
			200,
			this.user.token,
			{
				title: 'test2',
				testEntityRelated: [{}, {}, {}]
			}
		);

		const data = await this.api.get(
			'/api/resource/testEntityRelated',
			200,
			this.user.token,
			{ 'f.test_entity_id': inserted2.id }
		);
		assert.deepEqual(data, inserted2.testEntityRelated);
	});

	it('should find items with limit and offset', async function() {
		const data = await this.api.get(
			'/api/resource/testEntityRelated',
			200,
			this.user.token,
			{ limit: 2, offset: 1 }
		);

		const expected = [
			this.inserted.testEntityRelated[1],
			this.inserted.testEntityRelated[2]
		];
		assert.deepEqual(data, expected);
	});

	it('should order items', async function() {
		const data = await this.api.get(
			'/api/resource/testEntityRelated',
			200,
			this.user.token,
			{ order: '-id' }
		);

		const expected = [
			this.inserted.testEntityRelated[2],
			this.inserted.testEntityRelated[1],
			this.inserted.testEntityRelated[0]
		];
		assert.deepEqual(data, expected);
	});

	it('should update item', async function() {
		const expected = R.pipe(
			R.omit(['testEntityRelated']),
			R.assoc('title', 'test2')
		)(this.inserted);

		const updated = await this.api.put(
			`/api/resource/testEntity/${this.inserted.id}`,
			200,
			this.user.token,
			{
				title: 'test2'
			}
		);
		assert.deepEqual(updated, expected);

		const found = await this.api.get(
			`/api/resource/testEntity/${this.inserted.id}`,
			200,
			this.user.token
		);
		assert.deepEqual(found, expected);
	});

	it('should delete item', async function() {
		const inserted = await this.api.post(
			'/api/resource/testEntity',
			200,
			this.user.token,
			{
				title: 'test1'
			}
		);

		const itemUrl = `/api/resource/testEntity/${inserted.id}`;
		const deleted = await this.api.delete(itemUrl, 200, this.user.token);
		assert.deepEqual(deleted, inserted);

		await this.api.delete(itemUrl, 404, this.user.token);
		await this.api.get(itemUrl, 404, this.user.token);
		await this.api.put(itemUrl, 404, this.user.token, { title: 'test2' });
	});
});
