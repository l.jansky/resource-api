import * as dbTestConnection from './dbTestConnection';

before(async function() {
	await dbTestConnection.init(this);
});
