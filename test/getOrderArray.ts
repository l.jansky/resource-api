import chai from 'chai';
import getOrderArray from '../src/getOrderArray';

const assert = chai.assert;

describe('GetOrderArray', () => {
	it('should get order array from string', () => {
		const inputString = '-id,attr1,-attr2';

		const expected = [
			{
				by: 'id',
				desc: true
			},
			{
				by: 'attr1',
				desc: false
			},
			{
				by: 'attr2',
				desc: true
			}
		];

		const orderArray = getOrderArray(inputString);

		assert.deepEqual(orderArray, expected);
	});
});
