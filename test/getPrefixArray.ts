import chai from 'chai';
import getPrefixArray from '../src/getPrefixArray';

const assert = chai.assert;

describe('GetPrefixArray', () => {
	it('should get array by prefix', () => {
		const inputObject = {
			'f.s1': 'test1',
			'notF.s3': 'test3',
			'f.s2': 'test2',
			test: 'test'
		};

		const expected = [
			{
				prefix: 'f',
				key: 's1',
				value: inputObject['f.s1']
			},
			{
				prefix: 'f',
				key: 's2',
				value: inputObject['f.s2']
			}
		];

		const subObject = getPrefixArray('f')(inputObject);

		assert.deepEqual(subObject, expected);
	});
});
