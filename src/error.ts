class AppError {
	public message: string;
	public status: number;
	public data: any;

	public constructor(code, message, data) {
		this.message = message;
		this.status = code;
		this.data = data || null;
	}
}

export const getError = (code: number, message: string, data?: any) => {
	return new AppError(code, message, data);
};

export const throwError = (code: number, message: string, data?: any) => {
	throw getError(code, message, data);
};

export const notFound = (message?: string, data?: any) => {
	message = message || 'Not Found';
	throwError(404, message, data);
};

export const forbidden = (message?: string, data?: any) => {
	message = message || 'Forbidden';
	throwError(403, message, data);
};

export const unauthorized = (message?: string, data?: any) => {
	message = message || 'Unauthorized';
	throwError(401, message, data);
};

export const badRequest = (message?: string, data?: any) => {
	message = message || 'Bad request';
	throwError(400, message, data);
};

export default {
	badRequest,
	unauthorized,
	forbidden,
	notFound,
	throwError
};
