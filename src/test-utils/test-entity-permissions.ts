import { expect } from 'chai';
import { ApiTestUtil, TestLoggedUser } from './apiTestUtil';

export interface Expectations {
	read?: boolean;
	update?: boolean;
	list?: boolean;
	delete?: boolean;
	listWritable?: boolean;
}

export interface ApiContext {
	api?: ApiTestUtil;
}

export interface UserContext {
	user?: TestLoggedUser;
}

export interface EntityContext {
	resource: string;
	entity?: any;
}

export interface TestConfig {
	label: string;
	entityContext: EntityContext;
	ownerExpectations: Expectations;
	nonMemberExpectations: Expectations;
}

export const testEntityPermissionsWithApiContext = (api: ApiContext) => async (
	userContext: UserContext,
	entityContext: EntityContext,
	expectations: Expectations
) => {
	if (typeof expectations.read !== 'undefined') {
		it(`Should ${expectations.read ? '' : 'not '}read the entity`, async () => {
			const { user } = userContext;
			const { entity, resource } = entityContext;

			const result = await api.api.get(
				`/api/resource/${resource}/${entity.id}`,
				expectations.read ? 200 : 404,
				user.token
			);
			if (expectations.read) {
				expect(result).to.deep.equal(entity);
			}
		});
	}

	if (typeof expectations.list !== 'undefined') {
		it(`Should ${expectations.list ? '' : 'not '}list the entity`, async () => {
			const { user } = userContext;
			const { entity, resource } = entityContext;

			const results = await api.api.get(
				`/api/resource/${resource}`,
				200,
				user.token
			);

			const result = results.find(e => e.id === entity.id);
			if (expectations.list) {
				expect(result).to.deep.equal(entity);
			} else {
				expect(!!result).to.be.false;
			}
		});
	}

	if (typeof expectations.listWritable !== 'undefined') {
		it(`Should ${
			expectations.listWritable ? '' : 'not '
		}list the entity as writable`, async () => {
			const { user } = userContext;
			const { entity, resource } = entityContext;

			const results = await api.api.get(
				`/api/resource/${resource}`,
				200,
				user.token,
				{ writable: true }
			);

			const result = results.find(e => e.id === entity.id);
			if (expectations.listWritable) {
				expect(result).to.deep.equal(entity);
			} else {
				expect(!!result).to.be.false;
			}
		});
	}

	if (typeof expectations.update !== 'undefined') {
		it(`Should ${
			expectations.update ? '' : 'not '
		}update the entity`, async () => {
			const { user } = userContext;
			const { entity, resource } = entityContext;

			const result = await api.api.put(
				`/api/resource/${resource}/${entity.id}`,
				expectations.update ? 200 : 404,
				user.token,
				{
					title: 'updated'
				}
			);
			if (expectations.update) {
				expect(result).to.deep.equal({
					...entity,
					title: 'updated'
				});
			}
		});
	}

	if (typeof expectations.delete !== 'undefined') {
		it(`Should ${
			expectations.delete ? '' : 'not '
		}delete the entity`, async () => {
			const { user } = userContext;
			const { entity, resource } = entityContext;

			await api.api.delete(
				`/api/resource/${resource}/${entity.id}`,
				expectations.delete ? 200 : 404,
				user.token
			);
		});
	}
};
