import R from 'ramda';
import supertestRequest from 'supertest';
import * as acl from '../acl';
import { setPasswordForUser, generatePassword } from '../auth';
import { LoggedUser, Acl } from '../types';

export interface TestLoggedUser extends LoggedUser {
	token: string;
	publicAcl: Acl;
	privateAcl: Acl;
}

export interface ApiTestUtil {
	get: (
		uri: string,
		expect: number,
		token?: string,
		query?: any
	) => Promise<any>;
	post: (
		uri: string,
		expect: number,
		token?: string,
		data?: any,
		query?: any
	) => Promise<any>;
	put: (
		uri: string,
		expect: number,
		token?: string,
		data?: any,
		query?: any
	) => Promise<any>;
	delete: (
		uri: string,
		expect: number,
		token?: string,
		query?: any
	) => Promise<any>;
	login: (roles: any[]) => Promise<TestLoggedUser>;
	stop: () => void;
}

let userCounter = 0;

const login = (request, config) => async (roles = []) => {
	const userResource = config.services.getResource('user', [], {
		onInsertData: acl.getOnInsertData(config.services.getResource, null)
	});

	const email = `test${userCounter++}@test.cz`;
	const user: TestLoggedUser = await userResource.insertOne({
		email,
		userRole: roles.map(roleId => ({ role_id: roleId }))
	});

	const password = generatePassword();

	await setPasswordForUser<any>(
		config.services.getResource,
		user.id,
		true,
		password
	);

	const userInfo = await post(request, config)('/api/auth/login', 200, null, {
		email,
		password
	});

	const publicAcl = R.find(R.whereEq({ system: 1, title: 'public' }))(user.acl);
	const privateAcl = R.find(R.whereEq({ system: 1, title: 'private' }))(
		user.acl
	);

	return R.merge(user, {
		token: userInfo.token,
		publicAcl,
		privateAcl
	});
};

const get = (request, config) => (uri, expect, token, query) => {
	const r = request.get(uri).expect(expect);

	if (query) {
		r.query(query);
	}

	if (token) {
		r.set('Authorization', `Bearer ${token}`);
	}

	if (config?.appConfig?.auth?.apiKey) {
		r.set('x-api-key', config?.appConfig?.auth?.apiKey);
	}

	return r.then(res => res.body);
};

const post = (request, config) => (uri, expect, token, data, query?: any) => {
	const r = request
		.post(uri)
		.send(data)
		.expect(expect);

	if (query) {
		r.query(query);
	}

	if (token) {
		r.set('Authorization', `Bearer ${token}`);
	}

	if (config?.appConfig?.auth?.apiKey) {
		r.set('x-api-key', config?.appConfig?.auth?.apiKey);
	}

	return r.then(res => res.body);
};

const put = (request, config) => (uri, expect, token, data, query) => {
	const r = request
		.put(uri)
		.send(data)
		.expect(expect);

	if (query) {
		r.query(query);
	}

	if (token) {
		r.set('Authorization', `Bearer ${token}`);
	}

	if (config?.appConfig?.auth?.apiKey) {
		r.set('x-api-key', config?.appConfig?.auth?.apiKey);
	}

	return r.then(res => res.body);
};

const del = (request, config) => (uri, expect, token, query) => {
	const r = request.delete(uri).expect(expect);

	if (query) {
		r.query(query);
	}

	if (token) {
		r.set('Authorization', `Bearer ${token}`);
	}

	if (config?.appConfig?.auth?.apiKey) {
		r.set('x-api-key', config?.appConfig?.auth?.apiKey);
	}

	return r.then(res => res.body);
};

const getApiTestUtil = (app, config?: any): ApiTestUtil => {
	const server = app.listen();
	const request = supertestRequest(server);

	return {
		get: get(request, config),
		post: post(request, config),
		put: put(request, config),
		delete: del(request, config),
		login: login(request, config),
		stop: () => server.close()
	};
};

export default getApiTestUtil;
