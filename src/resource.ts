import R from 'ramda';
import { parse, getRelations, formatOne } from '@prejt/entity-formatter';
import getFilterRelations from './getFilterRelations';
import mergeRelations from './mergeRelations';
import {
	FindParams,
	Resource,
	FindFilter,
	DbStorage,
	Entity,
	EntityData,
	getStorage,
	Format,
	FormatObject,
	getResource,
	ID,
	RoleResource
} from './types';

const findAll = (storage: DbStorage, defaultFilter: FindFilter[]) => async (
	params: FindParams = {}
): Promise<EntityData[]> => {
	const format = params.format || {};
	const filter = R.concat<FindFilter>(defaultFilter, params.filter || []);
	const limit = params.limit || 0;
	const offset = params.offset || 0;
	const order = params.order || [];

	const formatObject: FormatObject =
		typeof format === 'string' ? parse(format) : format;
	const filterRelations = getFilterRelations(filter);
	const formatRelations = getRelations(formatObject);
	const relations = mergeRelations(filterRelations, formatRelations);

	const entities = await storage.find({
		filter,
		relations,
		limit,
		offset,
		order
	});
	const formatOneWithFormat = formatOne(formatObject);
	return R.map(formatOneWithFormat, entities);
};

const findOneById = (storage: DbStorage, defaultFilter: FindFilter[]) => async (
	id: ID,
	format: string | Format = '*'
): Promise<EntityData> => {
	const filter: FindFilter[] = [
		{
			field: 'id',
			operator: 'eq',
			value: id
		}
	];

	const entities = await findAll(
		storage,
		defaultFilter
	)({
		filter,
		format
	});

	return entities[0] || null;
};

const insertOne = (storage: DbStorage, onInsertData) => async (
	data: EntityData
): Promise<Entity> => {
	let insertData = data;
	if (onInsertData) {
		insertData = await onInsertData(data, storage);
	}

	return storage.insertOne(insertData);
};

const updateOneById = (storage: DbStorage, onUpdateData) => async (
	id: ID,
	data: EntityData
): Promise<Entity> => {
	let updateData = data;
	if (onUpdateData) {
		updateData = await onUpdateData(data, storage);
	}

	return storage.updateOneById(id, updateData);
};

const deleteOneById = (storage: DbStorage) => (id: ID): Promise<Entity> => {
	return storage.deleteOneById(id);
};

export const getCheckResourcePermission = (
	roleResource: RoleResource[],
	permissionFor: string = 'read'
) => (resourceName: string) => {
	return R.pipe<RoleResource[], boolean>(
		R.any(
			R.whereEq({
				resource: resourceName,
				[permissionFor]: 1
			})
		)
	)(roleResource);
};

const getResourceFactory = (getStorage: getStorage): getResource => (
	name: string,
	defaultFilter: FindFilter[] = [],
	events = {}
): Resource => {
	const storage = getStorage(name);
	if (!storage) {
		return null;
	}

	return {
		findAll: findAll(storage, defaultFilter),
		findOneById: findOneById(storage, defaultFilter),
		insertOne: insertOne(storage, events.onInsertData),
		updateOneById: updateOneById(storage, events.onUpdateData),
		deleteOneById: deleteOneById(storage)
	};
};

export default getResourceFactory;
