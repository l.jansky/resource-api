import { reduce } from 'ramda';
import { Relation } from './types';

const flattenRelationsToNames: (relations: Relation[]) => Set<string> = reduce<
	Relation,
	Set<string>
>((acc, relation) => {
	return new Set([
		...acc,
		relation.name,
		...flattenRelationsToNames(relation.relations || [])
	]);
}, new Set([]));

export default flattenRelationsToNames;
