import R from 'ramda';
import { parseFunction } from '@prejt/entity-formatter';
import getPrefixArray from './getPrefixArray';

const getFilterByFunction = (parsedFunction, field) => {
	switch (parsedFunction.name) {
		case 'like':
		case '_like':
		case 'like_':
		case 'gte':
		case 'lt':
		case 'lte':
			return {
				field,
				operator: parsedFunction.name,
				value: parsedFunction.params[0]
			};
		default:
			return null;
	}
};

export default R.pipe<any, any, any>(
	getPrefixArray('f'),
	R.map<any, any>(item => {
		const parsed = parseFunction(item.value);
		return parsed.params.length
			? getFilterByFunction(parsed, item.key)
			: {
					field: item.key,
					operator: 'eq',
					value: item.value
			  };
	})
);
