import KoaRouter from 'koa-router';
import koaBodyParser from 'koa-bodyparser';

import resource from './resource';
import acl from './acl';
import auth from './auth';
import publicApi from './public';
import passport from '../authentication/passport';

const router = new KoaRouter();

export default async function(customRouter) {
	// const configMiddleware = await config.getMiddleware();
	// router.use(configMiddleware);

	router.use('/api/auth', auth.routes());

	router.use(
		'/api/resource',
		passport.authenticate('jwt', { session: false }),
		resource.routes()
	);

	router.use(
		'/api/acl',
		passport.authenticate('jwt', { session: false }),
		acl.routes()
	);

	router.use('/api/public', publicApi.routes());

	if (customRouter) {
		router.use('/api/custom', koaBodyParser(), customRouter.routes());
	}

	return router.routes();
}
