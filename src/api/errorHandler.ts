export default async (ctx, next) => {
	try {
		await next();
	} catch (error) {
		const message = typeof error === 'string' ? error : error.message;
		ctx.services.logger &&
			ctx.services.logger.error(message, {
				correlationId: ctx.state.correlationId,
				error
			});

		ctx.status = error.status || 500;
		ctx.body = error;
	}
};
