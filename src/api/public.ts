import KoaRouter from 'koa-router';
import R from 'ramda';
import { parse, getRelations } from '@prejt/entity-formatter';
import * as error from '../error';
import getFilterArray from '../getFilterArray';
import getOrderArray from '../getOrderArray';
import flattenRelationsToNames from '../flattenRelationsToNames';
import { getPublicResource } from '../acl';
import { Resource, Role } from '../types';
import { getCheckResourcePermission } from '../resource';

const router = new KoaRouter();

const getPublicMiddleware = () => async (ctx, next) => {
	const resourceName = ctx.params.resource;

	const roleResource: Resource = ctx.services.getResource('role');
	const role = await roleResource.findOneById(
		ctx.services.getPublicRoleId(),
		'id,title,(roleResource)'
	);

	const storage = ctx.services.getStorage(resourceName);
	if (!storage) {
		error.notFound('Storage not found');
	}

	ctx.resource = getPublicResource(ctx.services.getResource)(
		storage.storageConfig
	);

	if (!ctx.resource) {
		error.notFound('Resource not found');
	}

	const checkResourcePermission = getCheckResourcePermission(
		((role as unknown) as Role).roleResource
	);

	const formatObject = ctx.query.format ? parse(ctx.query.format) : {};
	const formatRelations: string[] = [
		resourceName,
		...flattenRelationsToNames(getRelations(formatObject))
	];

	const allowed = R.pipe<string[], boolean[], boolean>(
		R.map(checkResourcePermission),
		R.all(R.identity)
	)(formatRelations);

	if (!allowed) {
		error.forbidden('Resource forbidden');
	}

	await next();
};

router.get('/:resource', getPublicMiddleware(), async function(ctx: any) {
	try {
		const format = ctx.query.format;
		const filter = getFilterArray(ctx.query);
		const limit = parseInt(ctx.query.limit) || 20;
		const offset = parseInt(ctx.query.offset) || 0;
		const order = ctx.query.order ? getOrderArray(ctx.query.order) : [];

		ctx.body = await ctx.resource.findAll({
			filter,
			format,
			limit,
			offset,
			order
		});
	} catch (err) {
		console.log('ERR', err);
	}
});

router.get('/:resource/:id', getPublicMiddleware(), async function(ctx: any) {
	try {
		const id = ctx.params.id;
		const format = ctx.query.format;

		const found = await ctx.resource.findOneById(id, format);
		if (!found) {
			error.notFound('Item not found');
		}

		ctx.body = found;
	} catch (err) {
		// console.log('ERR', err);
	}
});

export default router;
