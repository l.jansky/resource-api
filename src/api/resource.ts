import KoaRouter from 'koa-router';
import koaBodyParser from 'koa-bodyparser';
import R from 'ramda';
import { parse, getRelations } from '@prejt/entity-formatter';
import * as error from '../error';
import getFilterArray from '../getFilterArray';
import getOrderArray from '../getOrderArray';
import { getAclResource } from '../acl';
import { LoggedUser, UserRole, Role, RoleResource } from '../types';
import { getCheckResourcePermission } from '../resource';
import flattenRelationsToNames from '../flattenRelationsToNames';

const router = new KoaRouter();

const resourceMiddleware = (write: boolean) => async (ctx, next) => {
	const user: LoggedUser = ctx.state.user;
	const resourceName: string = ctx.params.resource;
	const writableOnly = ctx.query.writable === 'true' || write;

	const storage = ctx.services.getStorage(resourceName);
	if (!storage) {
		error.notFound('Storage not found');
	}

	// TODO: maybe not working for parent entities with acl
	ctx.resource = getAclResource(ctx.services.getResource)(user)(
		storage.storageConfig,
		writableOnly
	);
	if (!ctx.resource) {
		error.notFound('Resource not found');
	}

	const roleResources = R.pipe<
		UserRole[],
		Role[],
		RoleResource[][],
		RoleResource[]
	>(
		R.pluck('role'),
		R.pluck('roleResource'),
		(nestedResources: RoleResource[][]) =>
			R.flatten<RoleResource>(nestedResources)
	)(user.userRole);

	const checkResourcePermission = getCheckResourcePermission(
		roleResources,
		writableOnly ? 'write' : 'read'
	);
	const formatObject = ctx.query.format ? parse(ctx.query.format) : {};
	const formatRelations: string[] = [
		resourceName,
		...flattenRelationsToNames(getRelations(formatObject))
	];

	const allowed = R.pipe<string[], boolean[], boolean>(
		R.map(checkResourcePermission),
		R.all(R.identity)
	)(formatRelations);

	if (!allowed) {
		error.forbidden('Resource forbidden');
	}

	await next();
};

const loggerMiddleware = message => async (ctx, next) => {
	ctx.services.logger &&
		ctx.services.logger.info({
			message,
			metadata: {
				resource: ctx.params.resource,
				data: ctx.body,
				user: {
					id: ctx.state.user.id,
					email: ctx.state.user.email
				}
			}
		});
	await next();
};

router.get('/:resource', resourceMiddleware(false), async function(ctx: any) {
	try {
		const format = ctx.query.format;
		const filter = getFilterArray(ctx.query);
		const limit = parseInt(ctx.query.limit);
		const offset = parseInt(ctx.query.offset);
		const order = ctx.query.order ? getOrderArray(ctx.query.order) : [];

		ctx.body = await ctx.resource.findAll({
			filter,
			format,
			limit,
			offset,
			order
		});
	} catch (err) {
		console.log('ERR', err);
	}
});

router.get('/:resource/:id', resourceMiddleware(false), async function(
	ctx: any
) {
	try {
		const id = parseInt(ctx.params.id);
		const format = ctx.query.format;

		const found = await ctx.resource.findOneById(id, format);
		if (!found) {
			error.notFound('Item not found');
		}

		ctx.body = found;
	} catch (err) {
		// console.log('ERR', err);
	}
});

router.post(
	'/:resource',
	koaBodyParser(),
	resourceMiddleware(true),
	async function(ctx: any, next) {
		const inserted = await ctx.resource.insertOne(ctx.request.body);
		ctx.body = inserted;
		await next();
	},
	loggerMiddleware('Resource entity created')
);

router.put(
	'/:resource/:id',
	koaBodyParser(),
	resourceMiddleware(true),
	async function(ctx: any, next) {
		const id = parseInt(ctx.params.id);

		const found = await ctx.resource.findOneById(id);

		if (!found) {
			error.notFound('Item not found');
		}

		const updated = await ctx.resource.updateOneById(id, ctx.request.body);

		if (!updated) {
			error.notFound('Item not found');
		}

		ctx.body = updated;
		await next();
	},
	loggerMiddleware('Resource entity updated')
);

router.delete(
	'/:resource/:id',
	resourceMiddleware(true),
	async function(ctx: any, next) {
		const id = parseInt(ctx.params.id);

		const found = await ctx.resource.findOneById(id);
		if (!found) {
			error.notFound('Item not found');
		}

		const deleted = await ctx.resource.deleteOneById(id);

		if (!deleted) {
			error.notFound('Item not found');
		}

		ctx.services.logger &&
			ctx.services.logger.info('Resource item deleted', { deleted });

		ctx.body = deleted;
		await next();
	},
	loggerMiddleware('Resource entity deleted')
);

export default router;
