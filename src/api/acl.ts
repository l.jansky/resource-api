import KoaRouter from 'koa-router';
import koaBodyParser from 'koa-bodyparser';
import R from 'ramda';
import * as error from '../error';
import getFilterArray from '../getFilterArray';
import getOrderArray from '../getOrderArray';

const router = new KoaRouter();

const loggerMiddleware = message => async (ctx, next) => {
	ctx.services.logger &&
		ctx.services.logger.info({
			message,
			metadata: {
				resource: 'Acl',
				data: ctx.body,
				user: {
					id: ctx.state.user.id,
					email: ctx.state.user.email
				}
			}
		});
	await next();
};

const permissionOptions = [
	{
		value: 'none',
		label: 'None'
	},
	{
		value: 'member',
		label: 'Member'
	},
	{
		value: 'public',
		label: 'Public'
	}
];

router.get('/permission', ctx => {
	ctx.body = permissionOptions;
});

router.get('/permission/:id', ctx => {
	const id = ctx.params.id;
	const option = permissionOptions.find(opt => opt.value === id);
	ctx.body = option || {};
});

router.get('/user', async (ctx: any) => {
	try {
		const format = ctx.query.format;
		const filter = getFilterArray(ctx.query);
		const limit = parseInt(ctx.query.limit);
		const offset = parseInt(ctx.query.offset);
		const order = ctx.query.order ? getOrderArray(ctx.query.order) : [];

		const aclResource = ctx.services.getResource('aclUser');

		ctx.body = await aclResource.findAll({
			filter,
			format,
			limit,
			offset,
			order
		});
	} catch (err) {
		console.log('ERR', err);
	}
});

router.post(
	'/user',
	koaBodyParser(),
	async (ctx: any, next) => {
		const aclResource = ctx.services.getResource('aclUser');
		const inserted = await aclResource.insertOne(ctx.request.body);
		ctx.body = inserted;
		await next();
	},
	loggerMiddleware('User added to list')
);

router.delete(
	'/user/:id',
	async (ctx: any, next) => {
		const id = parseInt(ctx.params.id);
		const aclResource = ctx.services.getResource('aclUser');
		const deleted = await aclResource.deleteOneById(id);
		ctx.body = deleted;
		await next();
	},
	loggerMiddleware('User removed from list')
);

router.get('/', async (ctx: any) => {
	const user = ctx.state.user;
	const format = ctx.query.format;
	const limit = parseInt(ctx.query.limit) || 20;
	const offset = parseInt(ctx.query.offset) || 0;
	const order = ctx.query.order ? getOrderArray(ctx.query.order) : [];
	const aclResource = ctx.services.getResource('acl');

	const filter = [
		...getFilterArray(ctx.query),
		{
			operator: 'or',
			value: [
				{
					field: 'user_id',
					operator: 'eq',
					value: user.id
				},
				{
					field: 'aclUser',
					operator: 'has',
					value: [
						{
							operator: 'eq',
							field: 'user_id',
							value: user.id
						}
					]
				}
			]
		}
	];

	const items = await aclResource.findAll({
		filter,
		format,
		limit,
		offset,
		order
	});
	ctx.body = items;
});

router.get('/:id', async (ctx: any) => {
	const user = ctx.state.user;
	const id = parseInt(ctx.params.id);
	const aclResource = ctx.services.getResource('acl');
	const filter = [
		{
			field: 'id',
			operator: 'eq',
			value: id
		},
		{
			operator: 'or',
			value: [
				{
					field: 'user_id',
					operator: 'eq',
					value: user.id
				},
				{
					field: 'aclUser',
					operator: 'has',
					value: [
						{
							operator: 'eq',
							field: 'user_id',
							value: user.id
						}
					]
				}
			]
		}
	];

	const items = await aclResource.findAll({ filter });

	if (!items.length) {
		error.notFound();
	}

	let read = 'none';
	if (items[0].member_read) {
		read = 'member';
	}

	if (items[0].public_read) {
		read = 'public';
	}

	let write = 'none';
	if (items[0].member_write) {
		write = 'member';
	}

	if (items[0].public_write) {
		write = 'public';
	}

	ctx.body = {
		...items[0],
		read,
		write
	};
});

router.post(
	'/',
	koaBodyParser(),
	async (ctx: any, next) => {
		const user = ctx.state.user;
		const body = ctx.request.body;

		const acl = {
			title: body.title,
			member_read: body.read === 'member' || body.read === 'public',
			member_write: body.write === 'member' || body.write === 'public',
			public_read: body.read === 'public',
			public_write: body.write === 'public',
			user_id: user.id,
			acl_id: body.acl_id
		} as any;

		if (body.members) {
			acl.aclUser = body.members.map(userId => ({ user_id: userId }));
		}

		const aclResource = ctx.services.getResource('acl');
		ctx.body = await aclResource.insertOne(acl);
		await next();
	},
	loggerMiddleware('Acl created')
);

router.put(
	'/:id',
	koaBodyParser(),
	async (ctx: any, next) => {
		const user = ctx.state.user;
		const id = parseInt(ctx.params.id);

		const ownAcl = R.any(R.propEq('id', id), user.acl);

		if (!ownAcl) {
			error.forbidden('Is not own acl');
		}

		const body = ctx.request.body;

		const acl = {} as any;
		if (body.title) {
			acl.title = body.title;
		}

		if (body.acl_id) {
			acl.acl_id = body.acl_id;
		}

		if (body.read) {
			acl.member_read = body.read === 'member' || body.read === 'public';
			acl.public_read = body.read === 'public';
		}

		if (body.write) {
			acl.member_write = body.write === 'member' || body.write === 'public';
			acl.public_write = body.write === 'public';
		}

		const aclResource = ctx.services.getResource('acl');
		const item = await aclResource.updateOneById(ctx.params.id, acl);

		if (!item) {
			error.forbidden('Acl not updated');
		}

		ctx.body = item;
		await next();
	},
	loggerMiddleware('Acl updated')
);

export default router;
