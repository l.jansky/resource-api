import KoaRouter from 'koa-router';
import koaBodyParser from 'koa-bodyparser';
import R from 'ramda';

import * as error from '../error';
import { default as passport } from '../authentication/passport';
import * as auth from '../auth';
import { getResource } from '../types';
import { ID } from '@prejt/db-storage';

const router = new KoaRouter();

const getUserMenu = R.pipe(
	R.prop('userRole'),
	R.map((userRole: any) => userRole.role.roleMenu),
	R.reduce(R.concat, [] as any),
	R.sort((a: any, b: any) => a.weight - b.weight),
	R.pluck('menu'),
	R.uniq
);

router.get('/login', (ctx, next) => {
	return passport.authenticate('jwt', { session: false }, async (err, user) => {
		if (err || !user) {
			ctx.body = {};
		} else {
			ctx.body = {
				id: user.id,
				menu: getUserMenu(user)
			};
		}
	})(ctx, next);
});

router.post('/login', (ctx, next) => {
	return passport.authenticate('local', async (err, user) => {
		if (err || !user) {
			ctx.status = 401;
			ctx.body = { error: 1 };
		} else {
			const token = auth.generateAccessToken(user.id);
			ctx.body = {
				token,
				info: {
					id: user.id,
					menu: getUserMenu(user)
				}
			};
		}
	})(ctx, next);
});

const authMiddleware = passport.authenticate('jwt', { session: false });

router.put('/password', authMiddleware, koaBodyParser(), async (ctx: any) => {
	const { oldPassword, newPassword, newPasswordRepeat } = ctx.request.body;
	if (!oldPassword || !newPassword || newPassword !== newPasswordRepeat) {
		error.badRequest();
	}

	const {
		services: { getResource },
		state: { user: loggedUser }
	} = ctx;

	const user = (await auth.getUserVerifiedByPassword(
		getResource,
		loggedUser.email,
		oldPassword
	)) as any;
	if (!user || user.id !== loggedUser.id) {
		error.badRequest();
	}

	await auth.setPasswordForUser<[getResource, string, boolean, string]>(
		getResource,
		loggedUser.id,
		true,
		newPassword
	);

	ctx.body = {};
});

router.post('/password', koaBodyParser(), async ctx => {
	const {
		request: {
			body: { email }
		},
		services: { getResource, mailService }
	} = ctx;

	if (!email) {
		error.badRequest('E-mail is required');
	}

	const user = await auth.getUserByEmail(getResource, email);
	if (!user) {
		error.badRequest('User not found');
	}

	const password = auth.generatePassword();
	await auth.setPasswordForUser<[getResource, ID, boolean, string]>(
		getResource,
		user.id as ID,
		false,
		password
	);
	try {
		await mailService.sendMail(
			'New password generated',
			`Password is: ${password}`,
			email
		);
		ctx.body = { ok: 1 };
	} catch (err) {
		ctx.body = { ok: 0 };
	}
});

router.get(
	'/facebook',
	passport.authenticate('facebook', {
		session: false,
		scope: ['email']
	})
);

router.get(
	'/facebook/callback',
	passport.authenticate('facebook', {
		session: false
	}),
	async ctx => {
		// TODO: render page which closes itself as here: https://www.sitepoint.com/spa-social-login-google-facebook/
		const token = auth.generateAccessToken(ctx.state.user.id);
		ctx.redirect(`http://localhost:8080/?token=${token}`);
	}
);

export default router;
