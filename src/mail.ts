import nodemailer from 'nodemailer';
import { MailService, MailConfig } from './types';

const getMailFactory = ({
	from,
	...createTransportObject
}: MailConfig): MailService => {
	const transporter = nodemailer.createTransport(createTransportObject);

	const sendMail = async (
		subject: string,
		text: string,
		to: string
	): Promise<any> => {
		const mailOptions = {
			from,
			to,
			subject,
			text
		};

		return new Promise((resolve, reject) => {
			transporter.sendMail(mailOptions, (error, info) => {
				if (error) {
					reject(error);
				} else {
					resolve(info);
				}
			});
		});
	};

	return {
		sendMail
	};
};

export default getMailFactory;
