import { randomBytes, pbkdf2Sync } from 'crypto';
import R from 'ramda';
import { getResource, Entity, EntityData, FbUser } from './types';
import { ID } from '@prejt/db-storage';
import jwt from 'jsonwebtoken';

export const JWT_SECRET = process.env.JWT_SECRET;

interface HashedPasswordConfig {
	salt: string;
	password_hash: string;
	iterations: number;
}

export const generateAccessToken = (userId: ID): string => {
	const token = jwt.sign({ userId }, JWT_SECRET);
	return token;
};

export const generatePassword = (): string => {
	return Math.random()
		.toString(36)
		.substring(7);
};

const setPasswordForUserBase = async (
	getResource: getResource,
	userId: ID,
	resetByUser: boolean,
	password: string
): Promise<Entity> => {
	const loginPasswordResource = getResource('loginPassword');

	const filter = [
		{
			field: 'user_id',
			operator: 'eq',
			value: userId
		}
	];

	const [foundPasswordInfo] = await loginPasswordResource.findAll({ filter });
	const passwordInfo = {
		...hashPassword(password),
		user_id: userId,
		password_reset: resetByUser ? 1 : 0
	};

	if (foundPasswordInfo) {
		return loginPasswordResource.updateOneById(
			foundPasswordInfo.id as ID,
			passwordInfo
		);
	}
	return loginPasswordResource.insertOne(passwordInfo);
};

export const setPasswordForUser = R.curry(setPasswordForUserBase);

const getUserVerifiedByPasswordBase = async (
	getResource: getResource,
	email: string,
	password: string
): Promise<EntityData> => {
	const format =
		'id,email,(userRole,(role,(roleMenu,weight,(menu)))),(loginPassword)';
	const user = await getUserByEmail(getResource, email, format);

	if (user && user.loginPassword[0]) {
		const passwordInfo = user.loginPassword[0];
		if (verifyPassword(passwordInfo, password)) {
			return user;
		}
	}

	return null;
};

export const getUserVerifiedByPassword = R.curry(getUserVerifiedByPasswordBase);

export const hashPassword = (password: string): HashedPasswordConfig => {
	const salt = randomBytes(128).toString('base64');
	const iterations = 10000;
	const password_hash = pbkdf2Sync(
		password,
		salt,
		iterations,
		32,
		'sha512'
	).toString('hex');
	return { salt, password_hash, iterations };
};

const verifyPasswordBase = (
	hashedPasswordConfig: HashedPasswordConfig,
	passwordAttempt: string
): boolean => {
	return (
		hashedPasswordConfig.password_hash ==
		pbkdf2Sync(
			passwordAttempt,
			hashedPasswordConfig.salt,
			hashedPasswordConfig.iterations,
			32,
			'sha512'
		).toString('hex')
	);
};

export const verifyPassword = R.curry(verifyPasswordBase);

export const getUserByEmail = async (
	getResource: getResource,
	email: string,
	format: string = 'id,email,userRole.role.roleMenu.menu.*'
): Promise<EntityData> => {
	const userResource = getResource('user');

	const filter = [
		{
			field: 'email',
			operator: 'eq',
			value: email
		}
	];

	const users = await userResource.findAll({ filter, format });
	return users[0] || null;
};

export const getUserByFbId = async (
	getResource: getResource,
	fbId: string,
	format: string = 'id,email,userRole.role.roleMenu.menu.*'
): Promise<EntityData> => {
	const userResource = getResource('user');

	const filter = [
		{
			field: 'loginFacebook',
			operator: 'has',
			value: [
				{
					field: 'fb_id',
					operator: 'eq',
					value: fbId
				}
			]
		}
	];

	const users = await userResource.findAll({ filter, format });
	return users[0] || null;
};

export const getOrCreateFbUser = async (
	getResource: getResource,
	fbProfile: FbUser
): Promise<EntityData> => {
	const userResource = getResource('user');
	const loginFacebookResource = getResource('loginFacebook');
	let user = await getUserByEmail(
		getResource,
		fbProfile.email,
		'id,email,(loginFacebook)'
	);
	if (!user) {
		user = await userResource.insertOne({
			email: fbProfile.email,
			first_name: fbProfile.first_name,
			last_name: fbProfile.last_name
		});
	}

	if (!user.loginFacebook || !(user.loginFacebook as any[]).length) {
		const loginFacebook = await loginFacebookResource.insertOne({
			user_id: user.id,
			fb_id: fbProfile.id
		});

		user.loginFacebook = loginFacebook;
	}

	return user;
};
