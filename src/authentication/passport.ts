import passport from 'koa-passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as FacebookStrategy } from 'passport-facebook';
import { Strategy as JWTStrategy, ExtractJwt } from 'passport-jwt';
import {
	getUserVerifiedByPassword,
	getOrCreateFbUser,
	JWT_SECRET
} from '../auth';
import { AppConfig, FbUser } from '../types';

export const configure = (services, appConfig: AppConfig) => {
	const userResource = services.getResource('user');
	passport.use(
		new LocalStrategy(
			{
				usernameField: 'email',
				passwordField: 'password',
				session: false
			},
			async (email, password, cb) => {
				const user = await getUserVerifiedByPassword(
					services.getResource,
					email,
					password
				);
				return cb(null, user);
			}
		)
	);

	passport.use(
		new JWTStrategy(
			{
				jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
				secretOrKey: JWT_SECRET
			},
			async (jwtPayload, cb) => {
				const user = await userResource.findOneById(
					parseInt(jwtPayload.userId),
					'id,email,(userRole,(role,(roleResource),(roleMenu,weight,(menu)))),(acl)'
				);
				return cb(null, user);
			}
		)
	);

	if (appConfig.auth.facebook) {
		const { clientId, clientSecret } = appConfig.auth.facebook;
		passport.use(
			new FacebookStrategy(
				{
					clientID: clientId,
					clientSecret: clientSecret,
					callbackURL: `${appConfig.baseUri}/api/auth/facebook/callback`,
					profileFields: ['id', 'emails', 'name']
				},
				async (accessToken, refreshToken, profile, cb) => {
					const fbUser: FbUser = profile._json;
					const user = await getOrCreateFbUser(services.getResource, fbUser);
					return cb(null, user);
				}
			)
		);
	}
};

export default passport;
