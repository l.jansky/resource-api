import R from 'ramda';
import { Relation } from './types';

const mergeRelations = (
	relations1: Relation[],
	relations2: Relation[]
): Relation[] => {
	const relationsMap = {};
	const inputRelations = R.concat(relations1, relations2);
	for (const relation of inputRelations) {
		if (!relationsMap[relation.name]) {
			relationsMap[relation.name] = relation.relations || [];
		} else if (relation.relations) {
			relationsMap[relation.name] = mergeRelations(
				relationsMap[relation.name],
				relation.relations
			);
		}
	}
	return Object.keys(relationsMap).map(name => ({
		name,
		relations: relationsMap[name]
	}));
};

export default mergeRelations;
