import { Middleware } from 'koa';
import { v4 as uuidV4 } from 'uuid';

type GenerateId = () => string;

export const getCorrelationMiddleware = (
	generateId: GenerateId = uuidV4
): Middleware => async (ctx, next) => {
	const correlationId = ctx.get('x-correlation-id') || generateId();
	ctx.response.set('x-correlation-id', correlationId);
	ctx.set('x-correlation-id', correlationId);
	ctx.state.correlationId = correlationId;
	return next();
};
