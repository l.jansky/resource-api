export { default as appFactory } from './app-factory';
export { default as getResourceFactory } from './resource';
export { default as getMailFactory } from './mail';
export { default as getFilterArray } from './getFilterArray';
export { default as getOrderArray } from './getOrderArray';
export { default as passport } from './authentication/passport';
export { default as error } from './error';
export {
	default as getApiTestUtil,
	ApiTestUtil,
	TestLoggedUser
} from './test-utils/apiTestUtil';
export * from './types';

export const getCoreModelsPath = () => {
	return `${__dirname}/models`;
};
