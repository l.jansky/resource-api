const getMiddleware = services => {
	return async (ctx, next) => {
		ctx.services = services;
		await next();
	};
};

export default getMiddleware;
