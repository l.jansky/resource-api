import R from 'ramda';
import * as error from './error';
import {
	FindFilter,
	getResource,
	Entity,
	DbStorage,
	LoggedUser,
	ID,
	StorageConfig
} from './types';

enum PermissionType {
	member = 'member',
	public = 'public'
}

const getOperationFilter = (
	permissionType: PermissionType,
	write: boolean
): FindFilter[] =>
	R.concat(
		[
			{
				operator: 'eq',
				field: `${permissionType}_read`,
				value: 1
			}
		],
		write
			? [
					{
						operator: 'eq',
						field: `${permissionType}_write`,
						value: 1
					}
			  ]
			: []
	);

const getAclPathQuery = (path: string[]) => (
	query: FindFilter[]
): FindFilter => {
	const [field, ...restOfPath] = path;
	return {
		operator: 'and',
		field,
		value: restOfPath.length ? [getAclPathQuery(restOfPath)(query)] : query
	};
};

export const getPublicQuery = (
	storageConfig: StorageConfig,
	write: boolean
): FindFilter[] => {
	if (storageConfig.acl) {
		const baseQuery = getOperationFilter(PermissionType.public, write);
		return [getAclPathQuery(storageConfig.acl.split('.'))(baseQuery)];
	}
	return [];
};

export const getAclQuery = (
	storageConfig: StorageConfig,
	userId: ID,
	write?: boolean
): FindFilter[] => {
	const ownerQuery: FindFilter[] = [
		{
			operator: 'eq',
			field: 'user_id',
			value: userId
		}
	];

	const memberQuery = R.append<FindFilter>(
		{
			operator: 'has',
			field: 'aclUser',
			value: [
				{
					operator: 'eq',
					field: 'user_id',
					value: userId
				}
			]
		},
		getOperationFilter(PermissionType.member, write)
	);

	const publicQuery = getOperationFilter(PermissionType.public, write);

	const aclFilters = [ownerQuery, memberQuery, publicQuery];

	if (storageConfig.acl) {
		return [
			{
				operator: 'or',
				value: aclFilters.map(getAclPathQuery(storageConfig.acl.split('.')))
			}
		];
	}

	return [];
};

const getWriteAclFilter = userId => {
	return {
		operator: 'or',
		value: [
			{
				field: 'user_id',
				operator: 'eq',
				value: userId
			},
			{
				operator: 'and',
				value: [
					{
						field: 'aclUser',
						operator: 'has',
						value: [
							{
								operator: 'eq',
								field: 'user_id',
								value: userId
							}
						]
					},
					{
						field: 'member_write',
						operator: 'eq',
						value: 1
					}
				]
			}
		]
	};
};

export const getOnInsertData = (
	getResource: getResource,
	user: Entity
) => async (data: Entity, storage: DbStorage): Promise<Entity> => {
	let resData = data;

	// set users system acl when created
	if (storage.storageConfig.name === 'user') {
		resData = R.assoc(
			'acl',
			[
				{
					title: 'private',
					system: 1
				},
				{
					title: 'public',
					system: 1,
					public_read: 1,
					member_read: 1
				}
			],
			data
		);
	}

	// set acl_id to users private acl when creating entity related to acl
	// or allow only own acl if acl_id defined
	if (user && storage.storageConfig.relatedStorages.acl) {
		if (typeof data.acl !== 'undefined') {
			error.forbidden('Acl must be filled');
		}

		const aclResource = getResource('acl');

		if (typeof data.acl_id !== 'undefined') {
			const acl = await aclResource.findAll({
				filter: [
					{
						field: 'id',
						operator: 'eq',
						value: data.acl_id
					},
					getWriteAclFilter(user.id)
				]
			});

			if (acl.length === 0) {
				error.forbidden('Acl not found');
			}
		} else {
			const acl = await aclResource.findAll({
				filter: [
					{
						field: 'user_id',
						operator: 'eq',
						value: user.id
					},
					{
						field: 'system',
						operator: 'eq',
						value: 1
					},
					{
						field: 'title',
						operator: 'eq',
						value: 'private'
					}
				]
			});

			if (acl[0]) {
				resData = R.assoc('acl_id', acl[0].id, data);
			}
		}
	}

	return resData;
};

const getOnUpdateData = (getResource: getResource, user: Entity) => async (
	data: Entity,
	storage: DbStorage
): Promise<Entity> => {
	if (user && storage.storageConfig.relatedStorages.acl) {
		if (typeof data.acl !== 'undefined') {
			error.forbidden('Acl should be set');
		}

		const aclResource = getResource('acl');

		if (typeof data.acl_id !== 'undefined') {
			const acl = await aclResource.findAll({
				filter: [
					{
						field: 'id',
						operator: 'eq',
						value: data.acl_id
					},
					getWriteAclFilter(user.id)
				]
			});

			if (acl.length === 0) {
				error.forbidden('Acl should be found');
			}
		}
	}

	return data;
};

export const getAclResource = (getResource: getResource) => (
	user: LoggedUser
) => (storageConfig: StorageConfig, write: boolean = false) => {
	const resourceName = storageConfig.name;
	const aclQuery = getAclQuery(storageConfig, user.id, write);

	return getResource(resourceName, aclQuery, {
		onInsertData: getOnInsertData(getResource, user as any),
		onUpdateData: getOnUpdateData(getResource, user as any)
	});
};

export const getPublicResource = (getResource: getResource) => (
	storageConfig: StorageConfig
) => {
	const resourceName = storageConfig.name;
	return getResource(resourceName, getPublicQuery(storageConfig, false));
};
