import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import cors from 'koa-cors';
import getRoutes from './api';
import errorHandler from './api/errorHandler';
import getServicesMiddleware from './services-factory';
import {
	configure as passportConfigure,
	default as passport
} from './authentication/passport';
import { getCorrelationMiddleware } from './correlation-middleware';
import { AppConfig } from './types';
import { getLoggerMiddleware } from './logger-middleware';

let app = null;

interface AppFactoryParams {
	appConfig: AppConfig;
	services: any;
	api: any;
}

export default async function({ appConfig, services, api }: AppFactoryParams) {
	if (!app) {
		app = new Koa();
		const routes = await getRoutes(api);

		// todo: toto dat pak pryc
		app.use(
			cors({
				origin: '*'
			})
		);

		if (process.env.NODE_ENV !== 'test') {
			app.use(getLoggerMiddleware());
		}

		app.use(errorHandler);
		app.use(bodyParser());
		app.use(getCorrelationMiddleware());
		app.use(passport.initialize());
		passportConfigure(services, appConfig);
		app.use(getServicesMiddleware(services));
		app.use(routes);
	}

	return app;
}
