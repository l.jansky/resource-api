import R from 'ramda';
import { OrderConfig } from './types';

export default (inputString: string): OrderConfig[] => {
	return R.pipe(
		R.split(','),
		R.map(itemString => {
			const desc = itemString.charAt(0) === '-';
			const by = desc ? R.slice(1, Infinity, itemString) : itemString;
			return { by, desc };
		})
	)(inputString);
};
