import { FindFilter, Entity, EntityData, ID } from '@prejt/db-storage';
import { Format } from '@prejt/entity-formatter';
export {
	Relation,
	FindFilter,
	DbStorage,
	Entity,
	EntityData,
	getStorage,
	OrderConfig,
	KeyValue,
	ID,
	StorageConfig
} from '@prejt/db-storage';
export { Formatter, Format, FormatObject, Pair } from '@prejt/entity-formatter';

export interface FindParams {
	filter?: FindFilter[];
	format?: any;
	limit?: number;
	offset?: number;
	order?: any;
}

export interface Resource {
	findAll: (params?: FindParams) => Promise<EntityData[]>;
	findOneById: (id: ID, format?: Format | string) => Promise<EntityData>;
	insertOne: (data: EntityData) => Promise<Entity>;
	updateOneById: (id: ID, data: EntityData) => Promise<Entity>;
	deleteOneById: (id: ID) => Promise<Entity>;
}

export type getResource = (
	name: string,
	defaultFilter?: FindFilter[],
	events?: any
) => Resource;

export interface RoleResource {
	id: ID;
	resource: string;
	read: number;
	write: number;
}

export interface Role {
	id: ID;
	roleResource: RoleResource[];
}

export interface UserRole {
	role_id: ID;
	user_id: ID;
	id: ID;
	role: Role;
}

export interface Acl {
	id: ID;
	title: string;
	system: number;
	user_id: ID;
	public_read?: number;
	member_read?: number;
}

export interface LoggedUser {
	id: ID;
	email: string;
	userRole: UserRole[];
	acl: Acl[];
}

export interface MailService {
	sendMail: (subject: string, text: string, to: string) => Promise<any>;
}

export interface MailConfig {
	from: string;
	service: string;
	auth?: {
		user: string;
		pass: string;
	};
}

export interface AppConfig {
	baseUri: string;
	auth: AuthConfig;
}

export interface AuthConfig {
	facebook?: AuthFacebookConfig;
}

export interface AuthFacebookConfig {
	clientId: string;
	clientSecret: string;
}

export interface FbUser {
	id: string;
	email: string;
	first_name: string;
	last_name: string;
}
