import R from 'ramda';
import { KeyValue, Pair } from './types';

interface PrefixKeyValue<T> {
	prefix: string;
	key: string;
	value: T;
}

const getPrefixArray = <T>(prefix: string) => (
	object: KeyValue<T>
): PrefixKeyValue<T>[] =>
	R.pipe<KeyValue<T>, Pair<T>[], PrefixKeyValue<T>[], PrefixKeyValue<T>[]>(
		R.toPairs,
		R.map(pair => {
			const path = pair[0].split('.');
			return {
				prefix: path.shift(),
				key: path.join('.'),
				value: pair[1]
			};
		}),
		R.filter(
			(item: PrefixKeyValue<T>) => item.prefix === prefix && item.key !== ''
		)
	)(object);

export default getPrefixArray;
