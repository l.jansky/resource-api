import morgan, { FormatFn } from 'koa-morgan';

morgan.token('correlation-id', (req, res) => {
	const correlationHeader =
		req.headers['x-correlation-id'] || res.getHeader('x-correlation-id');
	return correlationHeader ? correlationHeader.toString() : undefined;
});

const formatJson: FormatFn = (tokens, req, res) =>
	JSON.stringify({
		message: 'HTTP request',
		url: tokens['url'](req, res),
		method: tokens['method'](req, res),
		correlationId: tokens['correlation-id'](req, res),
		status: tokens['status'](req, res),
		responseTime: tokens['response-time'](req, res),
		totalTime: tokens['total-time'](req, res),
		userAgent: tokens['user-agent'](req, res)
	});

export const getLoggerMiddleware = () => morgan(formatJson as any);
